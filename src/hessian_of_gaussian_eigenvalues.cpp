//[[Rcpp::depends(VH)]]
#include <Rcpp.h>
#include <vigra/multi_array.hxx>
#include <vigra/convolution.hxx>
#include <vigra/tensorutilities.hxx>
#include <vigra/mathutil.hxx>
#include <vigra/multi_tensorutilities.hxx>
using namespace Rcpp;
using namespace vigra;

//' Hessian of Gaussian eigenvalues
//'
//' Calculate eigenvalues of Hessian of Gaussian
//' @param x Numeric matrix
//' @param sigma Scale of smoothing
//' @return List of 2 matrices with 1st and 2nd eigenvalue
//' @export
// [[Rcpp::export]]
List  hessian_of_gaussian_eigenvalues(NumericMatrix x, double sigma) {
  const int nrows = x.nrow();
  const int ncols = x.ncol();
  Shape2 shape(nrows, ncols);
  MultiArrayView<2, double> input_image(shape, x.begin());
  MultiArray<2, TinyVector<float, 3> >  hessian(shape);
  MultiArray<2, TinyVector<float, 2> >  eigen(shape);
  hessianMatrixOfGaussian(input_image, hessian, sigma);
  tensorEigenvaluesMultiArray(hessian, eigen);
  MultiArrayView<2, float, StridedArrayTag> first = eigen.bindElementChannel(0);
  MultiArrayView<2, float, StridedArrayTag> second = eigen.bindElementChannel(1);
  NumericMatrix first_result(nrows, ncols);
  NumericMatrix second_result(nrows, ncols);
  std::copy(first.begin(), first.end(), first_result.begin());
  std::copy(second.begin(), second.end(), second_result.begin());
  return Rcpp::List::create(Rcpp::Named("ei1") = first_result,
                                     Rcpp::Named("ei2") = second_result);
}

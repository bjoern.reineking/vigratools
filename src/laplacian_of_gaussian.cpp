//[[Rcpp::depends(VH)]]
#include <Rcpp.h>
#include <vigra/multi_array.hxx>
#include <vigra/convolution.hxx>
using namespace Rcpp;
using namespace vigra;


//' Laplacian of Gaussian
//'
//' Calculates laplacian of Gaussian.
//' @param x Numeric matrix
//' @param sigma Scale of the Gaussian kernel
//' @return Numeric matrix; smoothed version of x
//' @export
// [[Rcpp::export]]
NumericMatrix laplacian_of_gaussian(NumericMatrix x, double sigma) {
  const int nrows = x.nrow();
  const int ncols = x.ncol();
  Shape2 shape(nrows, ncols);
  MultiArrayView<2, double> input_image(shape, x.begin());
  NumericMatrix result(nrows, ncols);
  MultiArrayView<2, double> output_image(shape, result.begin());
  laplacianOfGaussian(input_image, output_image, sigma);
  return(result);
}


